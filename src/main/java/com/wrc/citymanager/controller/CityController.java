package com.wrc.citymanager.controller;

import com.wrc.citymanager.pojo.City;
import com.wrc.citymanager.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * author wrc 2021 6 11
 */
@Controller
public class CityController {
    @Autowired
    private CityService cityService;

    @RequestMapping(value = "/*")//映射页面
    public String index(HttpServletRequest request) {
        List<City> cities = cityService.selectLevelFirst();
        request.setAttribute("selectLevelFirst", cities);
        return "index";
    }

    @RequestMapping(value = "/resLevelUp")//根据code查询下级
    @ResponseBody
    public Object resLevelUp(HttpServletRequest request, String code) {
        List<City> cities = cityService.selectLevelUp(code);
        return cities;
    }

    @RequestMapping(value = "/likeSelectName")//模糊查询
    @ResponseBody
    public Object likeSelectName(HttpServletRequest request, String name) {
        List<City> cities = cityService.likeSelectName(name);
        return cities;
    }


}
