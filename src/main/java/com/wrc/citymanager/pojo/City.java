package com.wrc.citymanager.pojo;


import java.io.Serializable;
import java.util.List;


public class City implements Serializable {

  private String id;//
  private String code;//省份代码',
  private String parentCode;//
  private String name;//省份名称
  private String shortName;//简称
  private String lng;//经度',
  private String lat;//纬度
  private String sort;// 排序

  private  City  levelOn;
  private List<City> levelUp;

  public City getLevelOn() {
    return levelOn;
  }

  public void setLevelOn(City levelOn) {
    this.levelOn = levelOn;
  }

  public List<City> getLevelUp() {
    return levelUp;
  }

  public void setLevelUp(List<City> levelUp) {
    this.levelUp = levelUp;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getParentCode() {
    return parentCode;
  }

  public void setParentCode(String parentCode) {
    this.parentCode = parentCode;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getShortName() {
    return shortName;
  }

  public void setShortName(String shortName) {
    this.shortName = shortName;
  }

  public String getLng() {
    return lng;
  }

  public void setLng(String lng) {
    this.lng = lng;
  }

  public String getLat() {
    return lat;
  }

  public void setLat(String lat) {
    this.lat = lat;
  }

  public String getSort() {
    return sort;
  }

  public void setSort(String sort) {
    this.sort = sort;
  }
}
