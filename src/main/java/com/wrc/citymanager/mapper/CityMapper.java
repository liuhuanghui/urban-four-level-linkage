package com.wrc.citymanager.mapper;

import com.wrc.citymanager.pojo.City;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * author wrc
 */
@Mapper
public interface CityMapper {
    @Select("SELECT * from city WHERE PARENT_CODE is null")
    List<City> selectLevelFirst();

    @Select("SELECT * from city WHERE PARENT_CODE ='${code}' ")
    List<City> selectByParentCode(@Param("code")String code);

    @Select("SELECT * from city WHERE CODE ='${code}'")
     City selectByCode(@Param("code")String code);

    @Select("SELECT * from city WHERE name  like '%${name}%' ")
    List<City> likeSelectName(@Param("name")String name);
}
