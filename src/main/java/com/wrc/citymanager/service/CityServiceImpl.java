package com.wrc.citymanager.service;

import com.wrc.citymanager.mapper.CityMapper;
import com.wrc.citymanager.pojo.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * author wrc
 */
@Service("cityService")
public class CityServiceImpl implements CityService {
    @Autowired
    private CityMapper cityMapper;

    @Override
    public List<City> selectLevelFirst() {
        return cityMapper.selectLevelFirst();
    }

    @Override
    public List<City> selectLevelUp(String code) {
        return cityMapper.selectByParentCode(code);
    }

    @Override
    public List<City> likeSelectName(String name) {
        List<City> likeSelectCities = cityMapper.likeSelectName(name);
        likeSelectCities.forEach(city -> {
            String code = city.getCode();
            String parentCode = city.getParentCode();
            City cityBean = cityMapper.selectByCode(parentCode);
            List<City> cities = cityMapper.selectByParentCode(code);
            city.setLevelOn(cityBean);
            city.setLevelUp(cities);
        });
        return likeSelectCities;
    }
}
