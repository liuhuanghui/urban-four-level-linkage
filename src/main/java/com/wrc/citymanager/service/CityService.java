package com.wrc.citymanager.service;

import com.wrc.citymanager.pojo.City;

import java.util.List;

/**
 * author wrc
 */
public interface CityService {
    /**
     * 省级
     * @return
     */
    List<City> selectLevelFirst();

    /**
     * 省级下的
     * @param code
     * @return
     */
    List<City> selectLevelUp(String code);


    /**
     * 模糊查询
     * @param name
     * @return
     */
    List<City> likeSelectName(String name);
}
